﻿#include <cstring>
#include <cstdio>
#define INITIAL_STACK_SIZE 2


class Stack
{
private:
    /* data */
    int* pdata;   // Указатель на массив, в котором будут храниться элементы стека.
    int capacity; // Ёмкость массива.
    int size;     // Текущий размер стека.
public:
    Stack(); // Конструктор
    ~Stack(); // Деструктор

    int pop(void);          // Возвращает верхний элемент стека.
    void push(int element); // Кладет элемент в стек.
    void printState(void);  // Выводит текущее состояние стека.
};

Stack::Stack()
{
    pdata = new int[INITIAL_STACK_SIZE];
    capacity = INITIAL_STACK_SIZE;
    size = 0;
};

Stack::~Stack()
{
    delete[] pdata; // Очистим память по старому указателю.
};

void Stack::push(int element)
{
    // Если наш массив заполнен - выделим новый, в 2 раза большего размера, и скопируем в него старый.
    if (size == capacity)
    {
        int* new_pdata = new int[2 * size]; // Выделяем память в 2 раза большего размера.
        memcpy(new_pdata, pdata, sizeof(int) * capacity); // Копируем старый стек в новую большую память
        capacity = 2 * size;                // Теперь максимальный размер в 2 раза больше.
        delete[] pdata; // Очистим память по старому указателю.
        pdata = new_pdata;
        printf("%s: inreasing capacity from %u to %u\r\n", __FUNCTION__, size, capacity);
    }

    pdata[size++] = element; // Кладем новый элемент в стек.
};


int Stack::pop(void)
{
    printf("%s: returns element = %u\r\n", __FUNCTION__, pdata[size - 1]);
    return pdata[--size];
};

void Stack::printState(void)
{
    printf("size = %u, capacity = %u\r\n", size, capacity);
    printf("stack data:\r\n    ");

    for (int i = 0; i < size; ++i)
    {
        printf("%u ", pdata[i]);

        if ((i + 1) % 10 == 0)
        {
            printf("\r\n    ");
        }
    }

    printf("\r\n");
};

int main()
{
    Stack myStack; // Объявили наш стек.

    /* Ниже вызываеи методы для проверки и периодически выводим состояние стека на экран */

    myStack.push(1);
    myStack.printState();
    myStack.push(2);
    myStack.push(3);
    myStack.push(4);
    myStack.push(5);
    myStack.printState();
    myStack.pop();
    myStack.pop();
    myStack.pop();
    myStack.pop();
    myStack.printState();

    for (int i = 0; i < 1000; ++i)
        myStack.push(i);

    myStack.printState();

};




